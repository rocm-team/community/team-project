# ROCm autopkgtests in containers

The following is a how-to for running ROCm autopkgtests in containers.

[[_TOC_]]


## Using Podman (rootless)


### TL;DR

1. Adjust `/etc/subgid` as described in [Prepping the host](#prepping-the-host)
2. Use the [rocm-autopkgtest](#autopkgtest-helper-script) script drive the
   tests (and build an image first, if necessary). This script automates all of the stuff below.

I really can't recommend a local APT cache enough.[^1]

### Background

On the host, regular users are granted access to the relevant devices by membership in groups `render` and `video`:

```shell
 $ ls -l /dev/kfd /dev/dri
crw-rw---- 1 root render 242, 0 Apr 10 18:05 /dev/kfd

/dev/dri:
total 0
drwxr-xr-x  2 root root         80 Apr 13 09:01 by-path
crw-rw----+ 1 root video  226,   0 Apr 13 09:01 card0
crw-rw----+ 1 root render 226, 128 Apr 10 18:05 renderD128
```

These devices can be passed into containers using the `--device` option to `podman-run(1)`. Given that regular users can access these devices only through the aforementioned group membership, the man page reminds us that we also need to specify `--group-add=keep-groups` when calling podman, so that the container process retains the invoking user's _host_ groups.

```shell
$ podman run --rm -it --device=/dev/dri --device=/dev/kfd debian:unstable
root@d1fdb8ddd9d2:/# test -r /dev/kfd && echo "OK" || echo "ERR"
ERR
root@d1fdb8ddd9d2:/# exit
$ podman run --rm -it --device=/dev/dri --device=/dev/kfd --group-add=keep-groups debian:unstable
root@de4f79a03625:/# test -r /dev/kfd && echo "OK" || echo "ERR"
OK
```

Getting `autopkgtest(1)` to run in rootless containers, however, is a bit tricky. This is because container testbeds have the `root-on-testbed` capability.[^2] In these testbeds, `autopkgtest` runs all tests through `su`, even those run by root, in order to ensure that every test has a PAM/logind session.

This `su` negates the effect of `--group-add=keep-groups` above by way of a `setgroups()` call. This is even the case when running `autopkgtest` with `--user root`, since we're not really root (who ignores groups), but a regular user that has been mapped to root in a user namespace.

```shell
$ podman run --rm -it --device=/dev/dri --device=/dev/kfd --group-add=keep-groups debian:unstable
root@de4f79a03625:/# test -r /dev/kfd && echo "OK" || echo "ERR"
OK
root@de4f79a03625:/# su
root@de4f79a03625:/# test -r /dev/kfd && echo "OK" || echo "ERR"
ERR
```

We work around this by (1) mapping the host groups into the user namespace of the container, and (2) adding these groups to the user with which the tests are run in the container.


### Prepping a container image

A suitable container image can be created using `autopkgtest-build-podman(1)`.

```shell
$ renderGID=`getent group render | cut -d: -f3`
$ autopkgtest-build-podman \
	--tag autopkgtest-rocm/debian:unstable \
	--post-command="groupadd -g $renderGID render; useradd -m -G render,video container-user" \
	--mirror http://N.N.N.N:9999/debian
```

1. `--tag autopkgtest-rocm/debian:unstable`. This is a customized container, so we want to differentiate it from a plain-vanilla autopkgtest container by name.

1. `--post-command="groupadd -g $renderGID render; useradd -m -G render,video container-user"`. We create the group `render` with a GID matching the host, then we create a user `container-user`,[^3] and add that user to the `render` and `video` groups. Later on, when starting the container, we will map these groups in the container to the same-named host groups.

1. `--mirror http://N.N.N.N:9999/debian`
Use a local apt cache.[^1] This is an optional but highly recommended step, as it _massively_ speeds up repeated tests. The IP address N.N.N.N of the cache server needs to be reachable from the container, so it can't be `localhost`.


### Prepping the host

We'll call the user running the autopkgtests `host-user`. Whatever user is used, must be a member of the `render` and `video` groups on the host.[^4]

```shell
$ sudo usermod -a -G render,video host-user
```

We need to set the list of subordinate GIDs that `host-user` may use in `/etc/subgid`. If `host-user` is a regular user, then there should already be an entry similar to this one:

```
host-user:100000:65536
```

It's fine if the start of the range differs from 100000. The important thing is that there is a sufficiently large range of subordinate GIDs available.

Assuming GIDs of `44(video)` and `(106)render` (**substitute 106 with the actual GID on your host!**), we modify `/etc/subgid` by adding entries for these two groups (this requires privileges):

```
host-user:100000:65536
host-user:44:1
host-user:106:1
```

After this,[^6] `host-user` will have access to subordinate GIDs `44`, `106`, and `100000-165535` (again, the latter range might differ and that's OK, you won't be using these directly).


### Mapping container groups to host groups

This is the trickiest part of the process. We need to define the host-to-container GID mappings using `--gidmap`, but specifying the right arguments is not exactly straightforward when using rootless Podman because it involves two mapping steps, rather than just one as Podman with root (see the [doc](https://docs.podman.io/en/latest/markdown/podman-run.1.html#uidmap-container-uid-from-uid-amount)).

The first mapping step is from **host** GIDs to **intermediate** GIDs. This mapping is constructed entirely from `/etc/subgid`. Given the `/etc/subgid` file we prepared above and assuming a GID `1000(host-user)`, the intermediate mapping would look like the following:

| hostGID         | intermediateGID | range | Comment                                           |
| ---:            | ---:            | ---:  | :---                                              |
|     1000        |               0 |     1 | GID of `host-user`, automatically added by Podman |
|       44        |               1 |     1 |                                                   |
|      106        |               2 |     1 |                                                   |
|   100000-165532 |         3-65535 | 65533 |                                                   |

The second mapping step is from **intermediate** GIDs to **container** GIDs. In the container, we assume that `container-user` has GID `1000(container-user)`, and we are certain of GIDs `44(video),106(render)`. Rows 1-3 of the following table present the mappings from host to container for these three groups.

In the container, this leaves the GID ranges 1-43, 45-105 and 107-65535 unmapped. Various parts of the system, like apt, need some of those IDs, and it's possible that tests might create groups and thus also need IDs and so on, so for good measure, we fill these gaps by mapping ranges. These are presented by rows 4-6 in the following table.

Note that with `--gidmap`, the order of arguments is: `containerGID:intermediateGID:range`.

| Row  | hostGID       | intermediateGID | range | containerGID | gidmap                   |
| ---: | ---:          | ---:            | ---:  | ---:         | :---                     |
|    1 |          1000 |               0 |     1 |            0 | `--gidmap=0:0:1`         | 
|    2 |            44 |               1 |     1 |           44 | `--gidmap=44:1:1`        |
|    3 |           106 |               2 |     1 |          106 | `--gidmap=106:2:1`       |
|    4 | 100000-100042 |            3-45 |    43 |         1-43 | `--gidmap=1:3:43`        |
|    5 | 100043-100103 |          46-107 |    61 |       45-105 | `--gidmap=45:46:61`      |
|    6 | 100104-165532 |       108-65535 | 65429 |    107-65535 | `--gidmap=107:108:65429` |

The result is that every container GID in the range 0-65535 is mapped to the host, with groups `0(root),44(video),106(render)` mapped to active groups of `host-user`.

Using the gidmaps above, access to the devices should be retained even after `su` to the test user:

```shell
$ podman run --rm -it \
	--device=/dev/dri \
	--device=/dev/kfd \
	--gidmap=0:0:1 \
	--gidmap=44:1:1 \
	--gidmap=106:2:1 \
        --gidmap=1:3:43 \
	--gidmap=45:46:61 \
	--gidmap=107:108:65429 \
	--group-add=keep-groups \
	autopkgtest-rocm/debian:unstable 
root@de4f79a03625:/# test -r /dev/kfd && echo "OK" || echo "ERR"
OK
root@de4f79a03625:/# su container-user
$ test -r /dev/kfd && echo "OK" || echo "ERR"
OK
```

### Running autopkgtests

Finally, the autopkgtests of source package `libfoo`, using source and binary packages from the official Archive, can be run as follows:

```shell
$ autopkgtest -B libfoo -- podman autopkgtest-rocm/debian:unstable -- \
	--device=/dev/dri \
	--device=/dev/kfd \
	--gidmap=0:0:1 \
	--gidmap=44:1:1 \
	--gidmap=106:2:1 \
        --gidmap=1:3:43 \
	--gidmap=45:46:61 \
	--gidmap=107:108:65429
```

1. `-B`. Don't build packages, get them from the Archive instead.

1. `--gidmap=X:X:X`. From the table above.

1. `--group-add=keep-groups`. This is no longer needed and thus omitted here, as it will always be undone anyway by the `su` when autopkgtest runs the test.

1. Note that we didn't need to tell autopkgtest to run tests with user `container-user`; the user was automatically picked up by virtue of its `1000<=UID<=59999`. This is a feature of `autopkgtest-virt-podman`.

Or, if one would like to run the tests of a local build result instead, with `.deb`s and `.dsc` in the current working directory:

```shell
$ autopkgtest -B *.deb *.dsc [...]
```


### autopkgtest helper script

[rocm-autopkgtest](../bin/rocm-autopkgtest) is a helper script that automates all of the above.

```shell
$ rocm-autopkgtest
Container image 'autopkgtest-rocm/debian:unstable' does not exist yet.
You can create one by running `rocm-autopkgtest --create`.
$ rocm-autopkgtest --create
Specify a mirror/apt cache (or leave blank): <local-apt-cache>
...
...

# From Archive
$ autopkgtest -B libfoo
# Local build result
$ autopkgtest -B *.deb *.dsc
# That's it!
```


## Using Podman (with root) or Docker


### Prepping a container image

This is very similar to xxx, except that no customization is needed. As before, having --mirror point to a local apt cache is optional but highly recommended.

```shell
$ autopkgtest-build-podman [--mirror http://N.N.N.N:9999/debian]
$ autopkgtest-build-docker [--mirror http://N.N.N.N:9999/debian]
```


### Remainder

TODO


## Footnotes

[^1]: Packages `approx`, `apt-cacher`, and `apt-cacher-ng` implement caching solutions. `approx`, in particular, is trivial to set up.

[^2]: See [Autopkgtest Virtualization Server Interface](https://salsa.debian.org/ci-team/autopkgtest/-/blob/master/doc/README.virtualisation-server.rst).

[^3]: Note that if you want to run tests as root within the container, you will need to add root to these groups, too (`usermod -a -G render,video root`). Remember: root is just root in the user namespace; root in the container is mapped to a regular user on the host. Within the container, `su root` will cause the container process to lose access to additional groups.

[^4]: Group `video` exists by Policy with GID=44. Group `render` should exist on hosts with `udev` installed, with a dynamically assigned GID from the system user range.

[^5]: The first range of 65536 IDs should be left as-is, as it appears to be a convention to this in first position. Tools like `unshare(1)` treat the first entry specially.

[^6]: Since Podman is daemon-less, this change may not have an immediate effect. I observed this information to be cached on times. I didn't find out _how/where_ it was cached, so I lazily solved the problem with a reboot.
