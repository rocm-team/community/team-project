[[_TOC_]]
***

# Introduction

Current ROCm version: 5.5.0

This is intended to be a place concentrating data about the packaging effort.

# Team policy
Please refer to the [policy of the Debian Science team](https://science-team.pages.debian.net/policy/).

# Package templates
**Disabling hardening and overriding lintian - template** \
https://wiki.debian.org/Hardening \
https://lintian.debian.org/tags \
https://lists.debian.org/debian-ai/2022/01/msg00025.html \
https://github.com/ROCm-Developer-Tools/HIP/blob/rocm-4.5.2/docs/markdown/clang_options.md \
The ROCm stack does not support --fstack-protector-strong for device-side code. \
All packages compiling .bc (LLVM IR bitcode), .hsaco (HSA LLVM code object) \
and .co files should have at least such hardening filtered from device compilation and any corresponding lintian \
overrides needed, tailored to the package.

In d/rules: `CXXFLAGS := $(subst -fstack-protector-strong,-Xarch_host -fstack-protector-strong,$(CXXFLAGS))`

```bash
myuser@debamd:/debamd$ cat debian/<binary_package_name>.lintian-overrides
# For ROCm source packages that will compile device-side code, either as
# LLVM IR (.bc files) or as GPU machine code (.hsaco and .co files),
# disable hardening for now.
# See also: https://lists.debian.org/debian-ai/2022/01/msg00025.html
<binary_package_name> binary: hardening-no-fortify-functions
<binary_package_name> binary: hardening-no-bindnow
<binary_package_name> binary: hardening-no-relro
```

## Security wonder
Will passing `hardening=-all` and not only the stack unprotection, lead to attack vectors \
that would not be there if only stack-protector strong was omitted? Not keen to see a mass \
vulnerability from WebGPU acceleration or whatever in the future... Ask for help?
Worrying for nothing? Maxime, 20220125

# [Qualification testing](./test_status.md)
Since Debian does not package the [AMD fork of LLVM](https://github.com/RadeonOpenCompute/llvm-project) at the moment ([more detail here](https://github.com/ROCm-Developer-Tools/HIP/issues/2449)), \
it is important to run internal testing thoroughly.

See the [ROCm autopkgtests in containers](doc/rocm-autopkgtests-in-containers.md) how-to for running autopkgtests in containers with rootless Podman, rootful Podman (TODO), and docker (TODO).

# TODO
## Outstanding points
The package **hipamd** as a whole, see below.

## Global passes
- We should tag debian package releases, for example with debian/4.5.2-1~exp1
- Add salsa-ci config files
- Find a test infrastructure, between dri-devel testfarm time slices, [AWS EC2](https://lists.debian.org/debian-ai/2022/02/msg00059.html), Debian buying/being donated GPUs and setting its own testfarm...
- copyright + symbol files in high-level packages
- clean various lintian output, notably override hardenings see # Package template
- revisit install paths according to [this mail dicsussion (bump discussion?)](https://lists.debian.org/debian-ai/2022/01/msg00036.html)
  - /usr/share/amdgcn/bitcode or /usr/lib/amdgcn/bitcode [WIP](https://lists.debian.org/debian-ai/2022/01/msg00099.html)
  - /usr/hip revisit install, -isystem stuff, hard **PENDING**
  - high-level libraries and general rocm-cmake recommandations [WIP](https://lists.debian.org/debian-ai/2022/01/msg00100.html)
- enable and report tests+benchmarks for as much packages as possible at ./test_status.md ^^
- continue building the Build-dep tree that M. Zhou initiated vv, later on report in d/control
- [Conflict pass in d/control for existing third-party packages](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1001712#10)
- ✅ ~~align on maintainer address in d/control, which translates in QA URL: [**debian-ai**](https://qa.debian.org/developer.php?email=debian-ai%40lists.debian.org) / [team+rocm-team](https://qa.debian.org/developer.php?email=team%2Brocm-team%40tracker.debian.org)~~
- ✅ ~~remove uupdate from d/watch files, `gbp import-orig --uscan` is sufficient if no dfsg~~.

## Per package
**HIPAMD**
  - Solve the [multi-GPU-arch-in-one-package topic](https://lists.debian.org/debian-ai/2021/12/msg00034.html)
  - [Fix dwz DWARF 5 support](https://lists.debian.org/debian-ai/2022/02/msg00058.html)

**rocgdb**
  - [Support for AMDGPU debugging is being upstreamed into GNU GDB](https://sourceware.org/pipermail/gdb-patches/2023-January/195361.html), so this package might not be necessary.

**rocsparse / hipsparse**
  - These packages need the SparseSuite Matrix Collection packaged in order to enable the tests by default. (see [post1](https://lists.debian.org/debian-ai/2022/10/msg00067.html), [post2](https://lists.debian.org/debian-ai/2022/12/msg00010.html))
  - The build will download the matricies if they're not provided, so it's still possible to uncomment the tests and build/run them when necessary.

## Library Size
- librocrand1 (5.3.3-1): 22 MiB
- librocsparse0 (5.3.3+dfsg-1): 1250 MiB
- librocfft0 (5.2.3-1~exp1): 5792 MiB
- librccl1 (5.3.3-1): 502 MiB
- hipcub: N/A
- rocthrust: N/A
- rocprim: N/A

# Supported Architectures
It may be worth considering building seperate binary packages for each architecture. The fat binaries for 5.2.3 and 5.3.3 support:

- gfx803
- gfx900
- gfx906
- gfx908
- gfx90a
- gfx1030

Most users likely only have one of these six GPUs, so 5/6th of the data in the library is probably unnecessary on their particular machine.
Maybe there should make rocfft a virtual package and have seperate binary packages for each architecture?
e.g., librocfft0-gfx906, librocfft0-gfx1030, etc? The library might be ~900 MiB rather than ~5700 MiB.

For more information on AMD GPU architecutres, see the [LLVM User Guide for AMDGPU Backend](https://llvm.org/docs/AMDGPUUsage.html).

# Dependency tree

1. level 1: components that have 0 ROCm dependency.  
    1. `src:roct-thunk-interface`  B-D:None  yields `bin:libhsakmt-dev`
    2. `src:rocm-cmake`  B-D:None  yields `bin:rocm-cmake`
    3. `src:rocm-smi-lib`  B-D:None yields `bin:liboam-dev, bin:librocm-smi-dev, bin:rocm-smi`
2. level 2: components depending on level 1  
    1. `src:rocm-device-libs`  B-D:rocm-cmake yields `rocm-device-libs`
3. level 3:
    1. `src:rocr-runtime` B-D:libhsakmt-dev,rocm-device-libs  yields `libhsa-runtime-dev`
    2. `src:rocm-compilersupport`  B-D:rocm-device-libs  yields `libamd-comgr-dev`
4. level 4:
    1. `src:rocminfo`   B-D:libhsa-runtime-dev,libhsamkt-dev yields `rocminfo`
5. level 5:
    1. `src:rocm-hipamd`  B-D:libamd-comgr-dev,libhsa-runtime-dev,librocm-smi-dev,rocminfo,rocm-device-libs yields `hipcc,libamdhip64-dev`
6. level 6:
    1. `src:rocrand`  B-D: hipcc,libamdhip64-dev yields `librocrand-dev, libhiprand-dev`
    1. `src:rocprim`  B-D: hipcc,libamdhip64-dev yields `librocprim-dev`
    1. `src:rocfft`  B-D: hipcc,libamdhip64-dev yields `librocfft-dev`
    1. `src:rocblas`  B-D: hipcc,libamdhip64-dev yields `librocblas-dev`
    1. `src:rccl`  B-D: hipcc,libamdhip64-dev yields `librccl-dev`
7. level 7:
    1. `src:rocsparse`  B-D: hipcc,libamdhip64-dev,librocprim-dev yields `librocsparse-dev`
    1. `src:hipcub`  B-D: librocprim-dev yields `libhipcub-dev`
    1. `src:rocthrust`  B-D: librocprim-dev yields `librocthrust-dev`
8. level 8:
    1. `src:hipsparse`  B-D: librocsparse-dev,libamdhip64-dev yields `libhipsparse-dev`
9. Deprecated packages
    1. `src:roc-smi`    Status:removed from experimental

See [The Road to PyTorch and Tensorflow on ROCm](https://lists.debian.org/debian-ai/2022/09/msg00029.html) for a continuation of this dependency tree.

# Packaging progress
The following table is a subset of the [main project data file](https://salsa.debian.org/rocm-team/community/team-project/-/blob/master/data/rocm_452_fourrezytout.csv):

Notes:
  - Found an OK workflow translating directly, without going through csv, from libre office calc ctrl+C to markdown using this [website](https://tableconvert.com/excel-to-markdown).
  - This table is made to track the packaging work, once the packages are uploaded, you can better inquire about their status in the [traditional QA page for the encompassing debian-ai team](https://qa.debian.org/developer.php?email=debian-ai%40lists.debian.org).
 
**Last update: Feb 14th, 2022**

| project              | region      | packaging status | amd binary package name | debian binary package name                            | upstream repo url                                           |
|----------------------|-------------|------------------|-------------------------|-------------------------------------------------------|-------------------------------------------------------------|
| rocm_smi_lib         | rocm        | 5.2.3 100%       | ?                       | librocm-smi64-1 librocm-smi-dev liboam1 liboam-dev    | https://github.com/radeonopencompute/rocm_smi_lib           |
| rocm-cmake           | rocm        | 5.4.0 100%       | rocm-cmake              | rocm-cmake                                            | https://github.com/radeonopencompute/rocm-cmake             |
| rocminfo             | rocm        | 5.2.3 100%       | ?                       | rocminfo                                              | https://github.com/radeonopencompute/rocminfo               |
| rocr-runtime         | rocm        | 5.2.3 100%       | hsa-runtime-rocr-amdgpu | libhsa-runtime64-1 libhsa-runtime-dev                 | https://github.com/radeonopencompute/rocr-runtime           |
| roct-thunk-interface | rocm        | 5.2.3 100%       | hsakmt-roct-amdgpu      | libhsakmt1 libhsakmt-dev                              | https://github.com/radeonopencompute/roct-thunk-interface   |
| rocm-compilersupport | llvm        | 5.0.0 100%       | comgr-amdgpu-pro        | libamd-comgr2 libamd-comgr-dev                        | https://github.com/radeonopencompute/rocm-compilersupport   |
| rocm-device-libs     | llvm        | 5.0.0 100%       | rocm-device-libs        | rocm-device-libs                                      | https://github.com/radeonopencompute/rocm-device-libs       |
| hip-examples         | hip         |                  |                         |                                                       | https://github.com/rocm-developer-tools/hip-examples        |
| hipamd               | hip         | 5.2.3 100%       | hip-rocr-amdgpu-pro     | libamdhip64-4 libamdhip64-dev libhiprtc-builtins-4    | https://github.com/rocm-developer-tools/hipamd              |
| opencl-icd-loader    | rocm-opencl | 5.0.0 40%        |                         |                                                       | https://github.com/khronosgroup/                            |
| rocm-opencl-runtime  | rocm-opencl | 5.0.0 40%        |                         |                                                       | https://github.com/radeonopencompute/rocm-opencl-runtime    |
| hipblas              | libraries   | 5.0.0 60%        |                         |                                                       | https://github.com/rocmsoftwareplatform/hipblas             |
| hipsparse            | libraries   | 5.3.3 100%       | hipsparse               | libhipsparse0 libhipsparse-dev                        | https://github.com/rocmsoftwareplatform/hipsparse           |
| rocalution           | libraries   |                  |                         |                                                       | https://github.com/rocmsoftwareplatform/rocalution          |
| rocblas              | libraries   | 5.3.3 85%        | rocblas                 | librocblas1 librocblas-dev                            | https://github.com/rocmsoftwareplatform/rocblas             |
| rocfft               | libraries   | 5.2.3 95%        | rocfft                  | librocfft0 librocfft-dev                              | https://github.com/rocmsoftwareplatform/rocfft              |
| rocmvalidationsuite  | libraries   |                  |                         |                                                       | https://github.com/rocm-developer-tools/rocmvalidationsuite |
| rocprim              | libraries   | 5.3.3 100%       | rocprim                 | librocprim-dev                                        | https://github.com/rocmsoftwareplatform/rocprim             |
| rocthrust            | libraries   | 5.3.3 90%        | rocthrust               | librocthrust-dev                                      | https://github.com/rocmsoftwareplatform/rocthrust           |
| rocrand              | libraries   | 5.3.3 100%       | rocrand                 | librocrand1 librocrand-dev libhiprand1 libhiprand-dev | https://github.com/rocmsoftwareplatform/rocrand             |
| rocsolver            | libraries   | 5.0.0 50%        | rocsolver               | librocsolver0 librocsolver-dev                        | https://github.com/rocmsoftwareplatform/rocsolver           |
| rocsparse            | libraries   | 5.3.3 100%       | rocsparse               | librocsparse0 librocsparse-dev                        | https://github.com/rocmsoftwareplatform/rocsparse           |
| rccl                 | libraries   | 5.3.3 95%        | rccl                    | librccl1 librccl-dev                        | https://github.com/rocmsoftwareplatform/rccl           |
| rocdbgapi            | gdb         |                  |                         |                                                       | https://github.com/rocm-developer-tools/rocdbgapi           |
| rocgdb               | gdb         |                  |                         |                                                       | https://github.com/rocm-developer-tools/rocgdb              |
| rocprofiler          | utilities   |                  |                         |                                                       | https://github.com/rocm-developer-tools/rocprofiler         |
| roctracer            | utilities   |                  |                         |                                                       | https://github.com/rocm-developer-tools/roctracer           |

