#!/bin/sh
# ROCm autopkgtest runner helper
#
# Copyright (c) 2023 Christian Kastner <ckk@kvr.at>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Usage:
#   `rocm-autopkgtest [options]`, where [options] are options to autopkgtest(1)
#   or
#   `rocm-autopkgtest --create`, to create a suitable image.
#  
# This script will run autopkgtests in using the autopkgtest-virt-podman
# virtualization server. It will configure autopkgtest-virt-podman such that
# the relevant devices are passed in, the relevant host groups are mapped in,
# etc.
#
# By default, this uses an image 'autopkgtest-rocm/debian:unstable' and will
# offer to create it if it doesn't exist yet. You can specify another image
# to use by setting exporting ROCM_AUTOPKGTEST_IMAGE=your_image_name.
#
# If you want to supply more arguments to the podman virtualization server,
# then export them as ROCM_AUTOPKGTEST_PODMAN_ARGS="foo bar baz".


##############
# Parameters #
##############

imageNAME=${ROCM_AUTOPKGTEST_IMAGE:-autopkgtest-rocm/debian:unstable}
userNAME=`whoami`
userGID=`id -g`
renderGID="`getent group render | cut -d: -f3`"
# By policy
videoGID=44


#############################
# Basic checks for the host #
#############################

if [ -z "$renderGID" ]
then
	echo -n "Group 'render' does not exist on this system. Are you " >&2
	echo -n "sure that you are on the right system? This group should " >&2
	echo "have been created by udev." >&2
	exit 1
elif ! groups "$userNAME" | grep -q '\brender\b'
then
	echo "'$userNAME' is not in group 'render'. Aborting." >&2
	exit 1
elif ! groups "$userNAME" | grep -q '\bvideo\b'
then
	echo "'$userNAME' is not in group 'video'. Aborting." >&2
	exit 1
elif [ "`cat /proc/sys/kernel/unprivileged_userns_clone`" != "1" ]
then
  	echo "unprivileged_userns_clone not enabled. Aborting. ">&2 
	exit 1
fi

# Assuming user=foo-user, renderGID=123, videoGID=44, we expect an /etc/subgid
# with these entries:
#
#     foo-user:44:1
#     foo-user:123:1
#     foo-user:nnnnnnnn:6553m
#
# nnnnnnnn:6553m is just a large range of subordinate GIDs that should have
# been allocated automatically when the user was created. The grep pattern is
# just a heuristic.
if ! grep -q "$userNAME:$renderGID:1" /etc/subgid ||
   ! grep -q "$userNAME:$videoGID:1" /etc/subgid ||
   ! grep -q -E "$userNAME:[0-9]{6,}:6553[4-6]" /etc/subgid
then
	echo -n "/etc/subgid is missing key subordinate GIDs. Refer to " >&2
	echo "the Wiki for instructions on how to set this up." >&2
	exit 1
fi


###########################
# Basic checks for Podman #
###########################

if [ ! -e /usr/bin/podman ]
then
	echo "podman is not installed on this system. Aborting." >&2
	exit 1
elif ! podman image exists "$imageNAME"
then
	if [ "$*" = "--create" ]
	then
		read -p "Specify a mirror/apt cache (or leave blank): " mirrorIN

		# This part should remain in sync with the Wiki
		postCMD="groupadd -g $renderGID render"
		postCMD="$postCMD; useradd -m -G render,video container-user"
		# So we don't need --group-add keep-groups
		postCMD="$postCMD; usermod -a -G render,video root"
		autopkgtest-build-podman \
			-t "$imageNAME" \
			--post-command="$postCMD" \
			`test -z "$mirrorIN" || echo "--mirror=$mirrorIN"`
		exit $?
	else
		echo "Container image '$imageNAME' does not exist yet." >&2
		echo "You can create one by running \`$0 --create\`." >&2
		exit 1
	fi
fi


######################
# autopkgtest runner #
######################

autopkgtest "$@" -- podman "$imageNAME" -- $ROCM_AUTOPKGTEST_PODMAN_ARGS \
	--device=/dev/dri \
	--device=/dev/kfd \
	--gidmap=0:0:1 \
	--gidmap=44:1:1 \
	--gidmap=$renderGID:2:1 \
	--gidmap=1:3:43 \
	--gidmap=45:46:$(($renderGID-$videoGID-1)) \
	--gidmap=$(($renderGID+1)):$(($renderGID+2)):65429
